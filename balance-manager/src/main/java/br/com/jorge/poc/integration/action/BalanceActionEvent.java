package br.com.jorge.poc.integration.action;

import lombok.*;

@Getter
@ToString
@AllArgsConstructor
@Builder(toBuilder = true)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BalanceActionEvent {

    private Integer cardId;

}
