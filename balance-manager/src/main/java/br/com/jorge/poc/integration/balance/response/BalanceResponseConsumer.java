package br.com.jorge.poc.integration.balance.response;

import br.com.jorge.poc.balance.BalanceService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
@EnableBinding(BalanceResponseChannel.class)
public class BalanceResponseConsumer {

    private final BalanceService balanceService;

    @StreamListener(target = BalanceResponseChannel.INPUT , condition = "headers['origin']=='balance-manager'")
    public void onReceiveMessage(@Payload BalanceResponseEvent balanceRequestEvent) {
        log.info("Balance received {}", balanceRequestEvent);
    }

}
