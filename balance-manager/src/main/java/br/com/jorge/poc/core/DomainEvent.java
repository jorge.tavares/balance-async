package br.com.jorge.poc.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

public interface DomainEvent {

    @JsonIgnore
    String origin();

    @JsonIgnore
    String destination();

    @JsonIgnore
    default String getEventType() {
        return this.getClass().getSimpleName();
    }

    @JsonIgnore
    default Message<DomainEvent> message(String traceId) {
        return MessageBuilder.withPayload(this)
                .setHeader("event-type", this.getEventType())
                .setHeader("origin", origin())
                .setHeader("destination", destination())
                .setHeader("traceId", traceId)
                .build();
    }
}
