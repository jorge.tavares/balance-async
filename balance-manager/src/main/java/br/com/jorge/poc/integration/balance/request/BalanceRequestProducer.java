package br.com.jorge.poc.integration.balance.request;

import brave.Tracer;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Slf4j
@Component
@AllArgsConstructor
@EnableBinding(BalanceRequestChannel.class)
public class BalanceRequestProducer {

    private final BalanceRequestChannel balanceRequestChannel;
    private final Tracer tracer;

    @TransactionalEventListener(BalanceRequestEvent.class)
    public void produce(BalanceRequestEvent balanceRequestEvent) {
        String traceId = tracer.currentSpan().toString();
        balanceRequestChannel.output().send(balanceRequestEvent.message(traceId));
        log.info("Balance request event published {}", balanceRequestEvent);
    }

}
