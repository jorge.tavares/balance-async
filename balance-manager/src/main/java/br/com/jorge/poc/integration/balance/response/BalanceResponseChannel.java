package br.com.jorge.poc.integration.balance.response;


import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface BalanceResponseChannel {

    String INPUT = "balance-response-queue";

    @Input(INPUT)
    SubscribableChannel input();

}
