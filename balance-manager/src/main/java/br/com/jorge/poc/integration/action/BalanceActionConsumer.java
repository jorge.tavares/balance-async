package br.com.jorge.poc.integration.action;


import br.com.jorge.poc.balance.BalanceService;
import br.com.jorge.poc.integration.balance.response.BalanceResponseEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
@EnableBinding(ActionChannel.class)
public class BalanceActionConsumer {

    private final BalanceService balanceService;

    @StreamListener(target = ActionChannel.INPUT , condition = "headers['action']=='balance'")
    public void onReceiveMessage(@Payload BalanceResponseEvent balanceRequestEvent) {
        balanceService.requestBalance(balanceRequestEvent.getCardId());
        log.info("Producing a Balance Request event {} ", balanceRequestEvent);
    }
}
