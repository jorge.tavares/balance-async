package br.com.jorge.poc.integration.balance.response;

import br.com.jorge.poc.core.BalanceManagerEvent;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@ToString
@AllArgsConstructor
@Builder(toBuilder = true)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BalanceResponseEvent extends BalanceManagerEvent {
    private Integer cardId;
    private Double value;
}
