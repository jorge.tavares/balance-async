package br.com.jorge.poc.integration.action;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface ActionChannel {

    String INPUT = "action-receive-queue";

    @Input(INPUT)
    SubscribableChannel input();
}
