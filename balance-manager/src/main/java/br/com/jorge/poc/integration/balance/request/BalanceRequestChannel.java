package br.com.jorge.poc.integration.balance.request;


import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface BalanceRequestChannel {

    String OUTPUT = "balance-request-topic";

    @Output(OUTPUT)
    MessageChannel output();

}
