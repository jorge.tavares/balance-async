package br.com.jorge.poc.integration.balance.request;

import br.com.jorge.poc.core.BalanceManagerEvent;
import br.com.jorge.poc.core.DomainEvent;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@ToString
@AllArgsConstructor
@Builder(toBuilder = true)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BalanceRequestEvent extends BalanceManagerEvent {
    private Integer cardId;
}
