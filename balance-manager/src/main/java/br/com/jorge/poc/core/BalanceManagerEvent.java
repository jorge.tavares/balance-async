package br.com.jorge.poc.core;

public abstract class BalanceManagerEvent implements DomainEvent {
    @Override
    public String origin() {
        return "balance-manager";
    }

    @Override
    public String destination() {
        return "balance";
    }
}
