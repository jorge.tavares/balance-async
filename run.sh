#!/bin/bash

function down_app_container() {
    echo "Stoping containers..."
    docker compose down
}

function delete_latest_balance_docker_image() {
    echo "Deleting docker image..."
    docker rmi -f balance:latest
}

function delete_latest_balance_manager_docker_image() {
    echo "Deleting docker image..."
    docker rmi -f balance-manager:latest
}

function delete_latest_balance_manager_docker_image() {
    echo "Deleting docker image..."
    docker rmi -f bot-orchestration:latest
}

function build_application() {
    echo "Building app..."
    ./mvnw clean install
}

function build_docker_image() {
    echo "Building docker image..."
    docker compose build
}

function up_app_container() {
    echo "Starting latest docker image..."
    docker compose up
}

#time (down_app_container)
#time (delete_latest_balance_docker_image)
#time (delete_latest_balance_manager_docker_image)
#time (build_application)
time (build_docker_image)
time (up_app_container)