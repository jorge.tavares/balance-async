package br.com.jorge.poc.integration.balance.response;


import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface BalanceResponseChannel {

    String OUTPUT = "balance-response-topic";

    @Output(OUTPUT)
    MessageChannel output();

}
