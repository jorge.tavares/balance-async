package br.com.jorge.poc.integration.balance.response;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.Map;

@Slf4j
@Component
@AllArgsConstructor
@EnableBinding(BalanceResponseChannel.class)
public class BalanceResponseProducer {

    private final BalanceResponseChannel balanceResponseChannel;

    @TransactionalEventListener(BalanceResponseEvent.class)
    public void produce(BalanceResponseEvent balanceResponseEvent) {
        balanceResponseChannel.output().send(MessageBuilder.withPayload(balanceResponseEvent)
                .copyHeaders(balanceResponseEvent.getHeaders())
                .build());
        log.info("Balance response published {}", balanceResponseEvent);
    }

}
