package br.com.jorge.poc.integration.balance.request;


import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface BalanceRequestChannel {

    String INPUT = "balance-request-queue";

    @Input(INPUT)
    SubscribableChannel input();

}
