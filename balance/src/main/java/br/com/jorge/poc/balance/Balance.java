package br.com.jorge.poc.balance;

import lombok.*;

import static lombok.AccessLevel.PRIVATE;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@Builder(toBuilder = true)
@NoArgsConstructor(access = PRIVATE)
public class Balance {
    private Double value;
    private Integer cardId;
}
