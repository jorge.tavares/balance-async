package br.com.jorge.poc.integration.balance.request;

import br.com.jorge.poc.balance.Balance;
import br.com.jorge.poc.balance.BalanceService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
@AllArgsConstructor
@EnableBinding(BalanceRequestChannel.class)
public class BalanceRequestConsumer {

    private final BalanceService balanceService;

    @StreamListener(target = BalanceRequestChannel.INPUT , condition = "headers['event-type']=='BalanceRequestEvent'")
    public void onReceiveMessage(@Payload BalanceRequestEvent balanceRequestEvent, @Headers Map<String, String> headers) {
        Integer cardId = balanceRequestEvent.getCardId();
        Balance balance = balanceService.getBalance(cardId);
        balanceService.publishBalance(headers, cardId, balance);
        log.info("Balance request event received {}, headers {}", balanceRequestEvent, headers.toString());
    }

}
