package br.com.jorge.poc.balance;

import java.util.Map;

public interface BalanceService {
    Balance getBalance(Integer cardId);
    Balance publishBalance(Map<String, String> headers, Integer cardId, Balance balance);
}
