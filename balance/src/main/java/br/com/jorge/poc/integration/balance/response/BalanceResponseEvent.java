package br.com.jorge.poc.integration.balance.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
@Getter
@ToString
@AllArgsConstructor
@Builder(toBuilder = true)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BalanceResponseEvent {
    private Integer cardId;
    private Double value;

    @JsonIgnore
    private Map<String, String> headers;
}
