package br.com.jorge.poc.balance;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
class BalanceRepository {

    public Optional<Balance> findBalanceByCardId(Integer cardId) {
        return findBalances().stream()
                .filter(balance -> balance.getCardId().equals(cardId))
                .findFirst();
    }

    private List<Balance> findBalances() {
        return Arrays.asList(Balance.builder()
                        .cardId(1)
                        .value(500.00)
                        .build(),
                Balance.builder()
                        .cardId(2)
                        .value(156.86)
                        .build(),
                Balance.builder()
                        .cardId(3)
                        .value(345.57)
                        .build()
                );
    }
}
