package br.com.jorge.poc.balance;

import br.com.jorge.poc.core.exception.ResourceNotFoundException;
import br.com.jorge.poc.integration.balance.response.BalanceResponseEvent;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Service
@AllArgsConstructor
class BalanceServiceImpl implements BalanceService {

    private final BalanceRepository balanceRepository;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Override
    public Balance getBalance(Integer cardId) {
        return balanceRepository.findBalanceByCardId(cardId)
                .orElseThrow(() -> new ResourceNotFoundException("Balance not found for Card " + cardId));
    }

    @Override
    @Transactional
    public Balance publishBalance(Map<String, String> headers, Integer cardId, Balance balance) {
        applicationEventPublisher.publishEvent(BalanceResponseEvent.builder()
                .cardId(cardId)
                .value(balance.getValue())
                .headers(headers)
                .build());
         return balance;
    }
}
