package br.com.jorge.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BotOrchestrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(BotOrchestrationApplication.class, args);
	}

}
