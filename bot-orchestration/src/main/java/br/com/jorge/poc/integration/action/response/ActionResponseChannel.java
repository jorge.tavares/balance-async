package br.com.jorge.poc.integration.action.response;


import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface ActionResponseChannel {

    String INPUT = "action-response-queue";

    @Input(INPUT)
    SubscribableChannel input();

}
