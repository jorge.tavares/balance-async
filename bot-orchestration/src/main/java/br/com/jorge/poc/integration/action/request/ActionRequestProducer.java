package br.com.jorge.poc.integration.action.request;

import brave.Tracer;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Slf4j
@Component
@AllArgsConstructor
@EnableBinding(ActionRequestChannel.class)
public class ActionRequestProducer {

    private final ActionRequestChannel actionRequestChannel;
    private final Tracer tracer;

    @TransactionalEventListener(BalanceActionEvent.class)
    public void produce(BalanceActionEvent balanceActionEvent) {
        String traceId = tracer.currentSpan().toString();
        actionRequestChannel.output().send(balanceActionEvent.message(traceId));
        log.info("Action balance event published {}", balanceActionEvent);
    }

}
