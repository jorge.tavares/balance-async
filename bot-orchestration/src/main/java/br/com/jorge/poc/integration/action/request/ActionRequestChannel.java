package br.com.jorge.poc.integration.action.request;


import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface ActionRequestChannel {

    String OUTPUT = "action-request-topic";

    @Output(OUTPUT)
    MessageChannel output();

}
