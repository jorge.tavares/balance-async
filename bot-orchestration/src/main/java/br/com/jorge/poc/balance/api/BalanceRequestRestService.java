package br.com.jorge.poc.balance.api;

import br.com.jorge.poc.balance.BalanceService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/balances")
class BalanceRequestRestService {

    private final BalanceService balanceService;

    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void requestBalance(@RequestBody BalanceRequest balanceRequest) {
        balanceService.requestBalance(balanceRequest.getCardId());
    }
}
