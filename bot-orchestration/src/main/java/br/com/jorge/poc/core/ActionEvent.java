package br.com.jorge.poc.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

public interface ActionEvent {

    @JsonIgnore
    String action();

    @JsonIgnore
    default String getEventType() {
        return this.getClass().getSimpleName();
    }

    @JsonIgnore
    default Message<ActionEvent> message(String traceId) {
        return MessageBuilder.withPayload(this)
                .setHeader("event-type", this.getEventType())
                .setHeader("action", action())
                .setHeader("traceId", traceId)
                .build();
    }
}
