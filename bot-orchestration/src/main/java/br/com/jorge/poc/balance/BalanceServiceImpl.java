package br.com.jorge.poc.balance;

import br.com.jorge.poc.integration.action.request.BalanceActionEvent;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
class BalanceServiceImpl implements BalanceService {

    private final ApplicationEventPublisher applicationEventPublisher;

    @Transactional
    public void requestBalance(Integer cardId) {
        applicationEventPublisher.publishEvent(BalanceActionEvent.builder()
                .cardId(cardId)
                .build());
    }
}
