package br.com.jorge.poc.balance;

public interface BalanceService {
    void requestBalance(Integer cardId);
}
