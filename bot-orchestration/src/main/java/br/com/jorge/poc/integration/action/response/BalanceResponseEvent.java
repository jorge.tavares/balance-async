package br.com.jorge.poc.integration.action.response;

import br.com.jorge.poc.core.ActionEvent;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@ToString
@AllArgsConstructor
@Builder(toBuilder = true)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BalanceResponseEvent implements ActionEvent {

    private Integer cardId;
    private Double value;

    @Override
    public String action() {
        return "balance";
    }
}
